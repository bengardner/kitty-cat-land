
// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.3 (Phaser v2.6.2)


/**
 * Title.
 */
function Title() {
	
	Phaser.State.call(this);
	
}

/** @type Phaser.State */
var Title_proto = Object.create(Phaser.State.prototype);
Title.prototype = Title_proto;
Title.prototype.constructor = Title;

Title.prototype.init = function () {
	
	this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.scale.pageAlignHorizontally = true;
	this.scale.pageAlignVertically = true;
	this.stage.backgroundColor = '#110028';
	
};

Title.prototype.preload = function () {
	
	this.load.pack('intro', 'assets/assets-pack.json');
	
	WebFontConfig = {
	    //  The Google Fonts we want to load (specify as many as you like in the array)
	    google: {
	      families: ['Poppins', 'Gurajada']
	    }
	};
	this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
	
};

Title.prototype.create = function () {
	var _background = this.add.tileSprite(0.0, 0.0, 2048.0, 1536.0, 'background_intro', null);
	
	var _title = this.add.sprite(960.0, 540.0, 'title');
	_title.scale.setTo(1.4, 1.4);
	_title.anchor.setTo(0.5, 0.5);
	
	var _play = this.add.sprite(960.0, 780.0, 'play');
	_play.anchor.setTo(0.5, 0.5);
	
	
	
	// fields
	
	this.fBackground = _background;
	this.fTitle = _title;
	this.fPlay = _play;
	this.myCreate();
	
};

/* --- end generated code --- */
// -- user code here --
Title.prototype.myCreate = function() {
	var audio = this.add.audioSprite("TitleAudio");
	var bgm = audio.get("Funky");
	bgm.play("Funky", null, 0.9, true);
	
	if (!this.transitioning) {
		this.tween = this.add.tween(this.fPlay.scale).to( { x: 0.9, y: 0.9 }, 200, Phaser.Easing.Elastic.In, true, 100, -1);
		this.tween.yoyo(true, 1600);
	}
	
	this.fPlay.inputEnabled = true;
	this.fPlay.events.onInputOver.add(function() {
		if (!this.transitioning) {
			this.tween.pause();
			this.add.tween(this.fPlay.scale).to( { x: 1, y: 1 }, 300, Phaser.Easing.Quartic.Out, true);
		}
	}, this);
	this.fPlay.events.onInputOut.add(function() {
		if (!this.transitioning) {
			this.add.tween(this.fPlay.scale).to( { x: 0.9, y: 0.9 }, 300, Phaser.Easing.Quartic.Out, true);
			this.tween.resume();
		}
	}, this);
	this.fPlay.events.onInputDown.add(function() {
		if (!this.transitioning) {
			audio.play("bell");
			bgm.fadeOut(900);
			this.startGame();
		}
	}, this);
};

Title.prototype.startGame = function() {
	this.transitioning = true;
	this.tween.stop();
	this.add.tween(this.fPlay.scale).to( { x: 0, y: 0 }, 300, Phaser.Easing.Quartic.InOut, true);
	this.add.tween(this.fTitle.scale).to( { x: 1, y: 1 }, 500, Phaser.Easing.Back.In, true).onComplete.addOnce(function() {
		this.add.tween(this.fTitle).to( { alpha: 0 }, 100, Phaser.Easing.Linear.None, true, 400);
	}, this);
	this.add.tween(this.fBackground).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
	this.time.events.add(1000, function (){
		this.game.state.start("Level");
	}, this);
};