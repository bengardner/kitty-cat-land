
// -- user code here --

/* --- start generated code --- */

// Generated by  1.5.3 (Phaser v2.6.2)


/**
 * Crystal
 * @param {Phaser.Game} aGame A reference to the currently running game.
 * @param {Number} aX The x coordinate (in world space) to position the Sprite at.
 * @param {Number} aY The y coordinate (in world space) to position the Sprite at.
 * @param {any} aKey This is the image or texture used by the Sprite during rendering. It can be a string which is a reference to the Cache entry, or an instance of a RenderTexture or PIXI.Texture.
 * @param {any} aFrame If this Sprite is using part of a sprite sheet or texture atlas you can specify the exact frame to use by giving a string or numeric index.
 */
function Crystal(aGame, aX, aY, aKey, aFrame) {
	Phaser.Sprite.call(this, aGame, aX, aY, aKey || 'crystal', aFrame == undefined || aFrame == null? 0 : aFrame);
	this.anchor.setTo(0.5, 0.5);
	var _anim_rotate = this.animations.add('rotate', [0, 1, 2, 3, 4, 5, 6, 7], 9, true);
	_anim_rotate.play();
	
}

/** @type Phaser.Sprite */
var Crystal_proto = Object.create(Phaser.Sprite.prototype);
Crystal.prototype = Crystal_proto;
Crystal.prototype.constructor = Crystal;

/* --- end generated code --- */
// -- user code here --
