function CatHealth (state, player, healthSprites) {
	this.state = state;
	this.player = player;
	this.healthSprites = healthSprites;
	
	// display health
	this.player.health = 5;
	
	this.spriteCoords = {
		x0: 8,
		y0: 8,
		dx: 36,
		dy: 0
	};

	for (var i = this.player.health - 1; i >= 0; i--) {
		var healthSprite = new HealthSprite(this.state.game, this.spriteCoords.x0 + i * this.spriteCoords.dx, this.spriteCoords.y0 + i * this.spriteCoords.dy);
		healthSprite.fixedToCamera = true;
		if (healthSprites) {
			healthSprites.add(healthSprite);			
		}
	}
}

CatHealth.prototype.update = function (enemies){
	// get hit
	this.state.physics.arcade.collide(enemies, this.player, this.playerEnemyHandler, null, this);
};

CatHealth.prototype.playerEnemyHandler = function(player) {
	if (player.invincible !== true) {
		this.getHit();
		player.play("injured");
		player.events.onAnimationComplete.addOnce(function() {
			player.play("idle");
			player.alpha = 0.5;
			this.state.add.tween(player).to( { alpha: 1 }, 2000, Phaser.Easing.Quartic.In, true);
			this.state.time.events.add(Phaser.Timer.SECOND * 2, function() {
				player.invincible = false;
			}, this);
		}, this);
	}
};

CatHealth.prototype.getHit = function(recoveryTime) {
	if (this.player.invincible !== true) {
		this.player.invincible = true;
		this.player.damage(1);
		this.state.sfx.play("ouch_cat");	
		this.healthSprites.getBottom().destroy();
		var flash = new HitFlash(this.state.game, this.player.x, this.player.y);
		flash.angle = Math.random() * 360;
		flash.tint = 0xff00ff;
		this.state.add.existing(flash);
		if (recoveryTime > 0) {
			this.player.alpha = 0.5;
			this.state.add.tween(this.player).to( { alpha: 1 }, Phaser.Timer.SECOND * recoveryTime, Phaser.Easing.Quartic.In, true);
			this.state.time.events.add(Phaser.Timer.SECOND * recoveryTime, function() {
				this.player.invincible = false;
			}, this);
		}
	}
};

CatHealth.prototype.addLife = function() {
	this.state.sfx.play("success");
	this.player.health += 1;
	var healthSprite = new HealthSprite(this.state.game, this.state.fHealthSprites.getBottom().x + this.spriteCoords.dx, this.spriteCoords.y0 + this.spriteCoords.dy);
	healthSprite.fixedToCamera = true;
	this.healthSprites.addAt(healthSprite, 0);
	this.state.add.tween(healthSprite.scale).from({x: 0, y: 0}, 1000, Phaser.Easing.Quartic.In, true);
};