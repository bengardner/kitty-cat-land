/**
 * @param {Phaser.Sprite} player 
 * @param {Phaser.State} state
 */
function CatMovement (state, player) {
	this.state = state;
	this.player = player;
	this.initPlayer = {
		speed: this.player.data.speed
	};
	this.boostAmount = 30;
	this.boosted = false;
	
	this.player.animations.getAnimation("jump_start").onComplete.add(function() { 
		this.play("jump_air");
	}, this.player);
	
	this.player.animations.getAnimation("jump_start_2").onComplete.add(function() { 
		this.play("jump_air");
	}, this.player);
}

CatMovement.prototype.update = function (ground){
	// update player velocity

	this.state.physics.arcade.collide(this.player, ground);

	var grounded = this.player.body.touching.down;
	if (grounded && this.boosted) {
		this.player.data.speed -= this.boostAmount;
		this.boosted = false;
	}

	var xVelocity = 0;

	if ((this.state.cursors.left.isDown || this.state.wasd.left.isDown) && this.player.animations.name !== "injured") {
		xVelocity = -this.player.data.speed;
		this.player.scale.x = -1;
	} else if ((this.state.cursors.right.isDown || this.state.wasd.right.isDown) && this.player.animations.name !== "injured") {
		xVelocity = this.player.data.speed;
		this.player.scale.x = 1;
	}

	this.player.body.velocity.x = xVelocity;

	// update this.player animation

	if (grounded) {
		if ((this.state.cursors.up.isDown || this.state.wasd.up.isDown) && this.player.animations.name !== "injured") {
			this.state.sfx.play("jump", 1);
			if (this.state.game.rnd.between(0,1))
			{
				this.player.body.velocity.y = -this.player.data.jumpHeight + this.boostAmount - 10;
				this.player.data.speed += this.boostAmount;
				this.boosted = true;
				this.player.play("jump_start_2");
				this.player.events.onAnimationComplete.addOnce(function() { if (this.boosted) this.player.data.speed -= this.boostAmount; this.boosted = false; }, this);
			} else {
				this.player.body.velocity.y = -this.player.data.jumpHeight;
				this.player.play("jump_start");
			}
		} else if (xVelocity == 0 && this.player.animations.name !== "injured") {
			this.player.play("idle");
		} else if (xVelocity != 0) {
			this.player.play("walk");
		}
	} else {
		//console.log("airborne");
	}
};