function CatCollecting (state, crystals, player, weapon) {
	this.state = state;
	this.crystals = crystals ? crystals : 0;
	this.player = player;
	this.weapon = weapon;
	this.onCollected = new Phaser.Signal();
}

CatCollecting.prototype.update = function (crystals){
	// collect crystals
	this.state.physics.arcade.overlap(this.player, crystals, this.playerCrystalHandler, null, this);
};

CatCollecting.prototype.playerCrystalHandler = function(player, crystal) {
	if (crystal.renderable)
	{
		this.state.sfx.play("coin", 0.6);
		crystal.kill();
		var flash = new DeathFlash(this.state.game, crystal.x, crystal.y);
		flash.tint = 0x00ffff;
		flash.scale.setTo(0.5, 0.5);
		this.state.add.existing(flash);
		this.crystals += 1;
		this.onCollected.dispatch(this.crystals);
		var tween = this.state.add.tween(this.state.fCollected).to( { tint: 0xff80ff }, 50, Phaser.Easing.Linear.None, true);
		tween.onComplete.addOnce(function() {
			tween.to({ tint: 0xffffff }, 150, Phaser.Easing.Quartic.Out, true);
		}, this.state);
		
		this.updatePower(this.crystals, this.weapon, this.player);
	}
};

CatCollecting.prototype.updatePower = function(p, weapon, player) {
	weapon.fireRate = this.state.math.clampBottom(this.state.fFriend.monAttacking.initBullet.rate - Math.sqrt(p) * 25, 0);
	weapon.fireRateVariance = this.state.math.clampBottom(this.state.fFriend.monAttacking.initBullet.variance - Math.cbrt(p) * 40, 0);
	weapon.bulletSpeed = this.state.fFriend.monAttacking.initBullet.speed + Math.cbrt(p) * 40;
	player.data.speed = player.catMovement.initPlayer.speed + p / 3.0;
	if (player.catMovement.boosted) {
		player.data.speed += player.catMovement.boostAmount;
	}
};