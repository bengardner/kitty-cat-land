window.onload = function() {
	var game = new Phaser.Game(1920, 1080);

	// Add the States your game has.	
	game.state.add("Title", Title);
	game.state.add("Level", Level);
	game.state.add("Boss", BossMap);

	game.state.start("Title");
};
