/**
 *
 */
function BossAttacking (state, boss) {
	this.state = state;
	this.x = function() { return boss.x; };
	this.y = function() { return boss.y + boss.height / 2; };
	// initial stat values
	this.initBullet = {
		rate: 125,
		variance: 75,
		speed: 1250
	};
	
	// weapons
	this.weapon = this.state.add.weapon(100, "energy_effect_base-0");
	this.weapon.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
	this.weapon.bulletSpeed = this.initBullet.speed;
	this.weapon.bulletSpeedVariance = 50;
	this.weapon.bulletGravity.y = -this.state.physics.arcade.gravity.y;
	this.weapon.fireRate = this.initBullet.rate;
	this.weapon.fireRateVariance = this.initBullet.variance;
	var fireSize = 16;
	this.weapon.fireFrom = new Phaser.Rectangle(this.x() - fireSize / 2, this.y() - fireSize / 2, fireSize, fireSize);
	this.weapon.trackSprite(boss, 0, 0, true);
	this.weapon.addBulletAnimation("hit", [4,5,6], 30, false);
	this.weapon.addBulletAnimation("fire", [0,1,2,3], 30, true);
	this.weapon.bullets.forEach(function(b) {
		b.health = 1;
		b.power = 1;
//	    b.scale.setTo(0.5, 0.5);
//	    b.body.updateBounds();
	}, this);
//	this.weapon.range = 150;
	this.weapon.onFire.add(function () {
		this.sfx.play("shot", 0.6 + Math.random() * 0.2);
	}, this.state);
	this.weapon.collisionHandler = function(enemy, bullet) {
		bullet.kill();
		if (enemy.catHealth !== undefined) {
			enemy.catHealth.getHit(1);
		} else {
			enemy.damage(1);
		}
	};
	this.spikes = this.state.add.group();
	this.state.time.events.repeat(Phaser.Timer.SECOND * 1, 999, function() {
		this.fireSpike();
	}, this);
}

BossAttacking.prototype.update = function(enemy) {
	this.weapon.fireAtSprite(enemy);
	this.state.physics.arcade.overlap(enemy, this.weapon.bullets, this.weapon.collisionHandler);
	this.state.physics.arcade.overlap(enemy, this.spikes, function(cat, spike) {
		spike.kill();
		cat.catHealth.playerEnemyHandler(cat);
	});
};

BossAttacking.prototype.fireSpike = function() {
	var spike = new GroundSpike(this.state.game, this.x(), this.y());
	spike.fire(true);
	this.spikes.add(spike);
};