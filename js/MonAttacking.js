/**
 * @param {Phaser.State} state 
 *
 */
function MonAttacking (state, mon, player) {
	this.state = state;
	this.mon = mon;
	this.player = player;
	this.lastSeenEnemies = [];
	
	// initial stat values
	this.initBullet = {
			rate: 500,
			variance: 300,
			speed: 125
	};
	
	// this.weapon
	this.weapon = this.state.add.weapon(100, "energy_effect_base-0");
	this.weapon.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
	this.weapon.bulletSpeed = this.initBullet.speed;
	this.weapon.bulletSpeedVariance = 50;
	this.weapon.bulletGravity.y = -this.state.physics.arcade.gravity.y;
	this.weapon.fireRate = this.initBullet.rate;
	this.weapon.fireRateVariance = this.initBullet.variance;
	this.weapon.trackSprite(this.mon, 0, 0, true);
	this.weapon.addBulletAnimation("hit", [28,29,30], 30, false);
	this.weapon.addBulletAnimation("fire", [24,25,26,27], 30, true);
	this.weapon.bullets.forEach(function(b) {
		b.health = 1;
		b.power = 1;
	    b.scale.setTo(0.5, 0.5);
	    b.body.updateBounds();
	}, this);
	this.weapon.range = 150;
	this.weapon.onFire.add(function () {
		this.sfx.play("shot", 0.6 + Math.random() * 0.2);
	}, this.state);
	this.mon.kills = 0;
	
	this.state.input.activePointer.leftButton.onDown.add(function() {
		if (!this.checkPlayerRange()) {
			var panicked = new Phaser.Text(this.state.game, -5, -64, "!", {"font":"bold 34px Tw Cen MT, Poppins","fill":"#ff0000","stroke":"#ffffff","strokeThickness":3});
			this.mon.addChild(panicked);
			this.state.add.tween(panicked).to({y: panicked.y - 8, alpha: 0}, 1000, Phaser.Easing.Quartic.In, true).onComplete.add(panicked.destroy, panicked);
			var pointing = new Phaser.Text(this.state.game, -10, -64, "↓", {"font":"bold 34px MoolBoran, Gurajada","fill":"#ff0000","stroke":"#ffffff","strokeThickness":3});
			this.player.addChild(pointing);
			this.state.add.tween(pointing).to({y: [pointing.y - 4, pointing.y + 4, pointing.y - 4, pointing.y + 4, pointing.y - 8], alpha: [1, 1, 0]}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(pointing.destroy, pointing);
		} else if (!this.getNearestEnemy()) {
			var confused = new Phaser.Text(this.state.game, -10, -64, "?", {"font":"bold 34px Tw Cen MT, Poppins","fill":"#0000ff","stroke":"#ffffff","strokeThickness":3});
			this.mon.addChild(confused);
			this.state.add.tween(confused).to({y: confused.y - 8, alpha: 0}, 1000, Phaser.Easing.Quartic.In, true).onComplete.add(confused.destroy, confused);
		}
	}, this);
	
	this.mon.events.onAnimationComplete.add(function() { this.mon.play("idle"); }, this);
}

MonAttacking.prototype.getNearestEnemy = function(nearbyEnemies) {
	var enemies = nearbyEnemies;
	if (nearbyEnemies === undefined) {
		enemies = this.lastSeenEnemies;
	} else {
		this.lastSeenEnemies = nearbyEnemies;
	}
	var nearest = null;
	var minDist = Number.MAX_SAFE_INTEGER;
	enemies.forEach(function(e) {
		if (e.alive && (dist = this.state.physics.arcade.distanceBetween(this.mon, e)) < minDist) {
			nearest = e;
			minDist = dist;
		}
	}, this);
	
	if (minDist < this.weapon.range) {
		return nearest;
	}
	return null;
};

MonAttacking.prototype.checkPlayerRange = function() {
	return (this.state.physics.arcade.distanceBetween(this.mon, this.player) < this.weapon.range);
};

MonAttacking.prototype.update = function(enemies) {
	// update this.weapon

//	var angle = this.physics.arcade.angleToPointer(this.fFriend);
//	this.weapon.bulletAngleOffset = this.math.radToDeg(angle);
//	this.weapon.trackOffset.x = 32*Math.cos(angle);
	this.weapon.trackOffset.y = -15; //32*Math.sin(angle);

	// ensure mon is within min distance to player and enemy
	var enemy = this.getNearestEnemy(enemies);
	if (this.state.input.activePointer.leftButton.isDown && enemy != null && this.checkPlayerRange()) {
		this.weapon.fireAtSprite(enemy);
		this.mon.play("attack");
	}
	
	// update this.mon movement

	//this.physics.arcade.collide(this.fFriend, this.fGround);
	
	this.state.physics.arcade.moveToPointer(this.mon, null, this.state.input.activePointer, 200);
	
	if (this.mon.body.velocity.x > 0)
	{
		this.mon.scale.x = 1;
	} else if (this.mon.body.velocity.x < 0)
	{
		this.mon.scale.x = -1;
	}
	
	this.state.physics.arcade.overlap(this.weapon.bullets, this.state.fGround, this.bulletGroundHandler, null, this);
};

MonAttacking.prototype.bulletEnemyHandler = function(enemy, bullet) {
	if (bullet.collided !== true) {
		bullet.collided = true;
		enemy.damage(bullet.power);
		var hitText = this.state.add.text(bullet.x - 9, bullet.y - 14, bullet.power, {"font":"italic 24px MoolBoran, Gurajada","fill":"#f0e080","stroke":"#c000f0","strokeThickness":3});
		this.state.add.tween(hitText).to({y: hitText.y - 8, alpha: 0}, 500, Phaser.Easing.Quartic.In, true);
		if (enemy.health > 0) {
			this.state.sfx.play("ouch", 0.6 + Math.random() * 0.2 * Math.cbrt(bullet.power));
			var flash = new HitFlash(this.state.game, enemy.x, enemy.y);
			flash.angle = Math.random() * 360;
			this.state.add.existing(flash);
		} else {
			this.state.sfx.play("hit");
			this.state.add.existing(new DeathFlash(this.state.game, enemy.x, enemy.y));
			this.mon.kills += 1;
			if (this.state.killsText) {
				this.state.killsText.text = this.mon.kills;
				if (this.mon.kills % 10 === 0) {
					this.player.catHealth.addLife();
					var tween = this.state.add.tween(this.state.killsText).to( { x: '+128' }, 300, Phaser.Easing.Linear.None, true);
					tween.onComplete.addOnce(function() {
						tween.to( { x: this.state.killsText.origin.x }, 1500, Phaser.Easing.Bounce.Out, true);
					}, this);
				} else {
					var tween = this.state.add.tween(this.state.killsText).to( { y: '+8' }, 20, Phaser.Easing.Linear.None, true);
					tween.onComplete.addOnce(function() {
						tween.to( { y: this.state.killsText.origin.y }, 180, Phaser.Easing.Quartic.Out, true);
					}, this);
				}				
			}
		}
		bullet.body.velocity = new Phaser.Point(0, 0);
		bullet.play("hit");
		bullet.events.onAnimationComplete.add(function() {
			bullet.collided = false;
			bullet.damage(1);
		});
	}
};

MonAttacking.prototype.bulletGroundHandler = function(bullet, ground) {
	if (bullet.collided !== true) {
		this.state.sfx.play("bogie", 0.25);
		bullet.collided = true;
		bullet.body.velocity = new Phaser.Point(0, 0);
		bullet.play("hit");
		bullet.events.onAnimationComplete.add(function() {
			bullet.collided = false;
			bullet.kill();
		});
	}
};